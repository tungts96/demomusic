package com.tungts.music.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.tungts.music.R;
import com.tungts.music.activity.MainActivity;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.model.MediaPlayerManager;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;

/**
 * Created by tungts on 7/31/2017.
 */

public class PlayingQueueFragment  extends Fragment {

    View root;
    MusicManager musicManager;
    MediaPlayerManager mediaPlayerManager;
    SeekBar seekBar;

    RecyclerView rcvPlayingQueue;
    ListAdapter plaingQueueAdapter;
    ArrayList<Song> arrCurrentSongPlay;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e("Fragment","onCreateView");
        root = inflater.inflate(R.layout.fragment_playing_queue,container,false);
        musicManager = new MusicManager(getContext());
        innitRecycleview();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("Fragment","onResume");
    }

    private void innitRecycleview() {
        rcvPlayingQueue = (RecyclerView) root.findViewById(R.id.rcvPlayingQueue);
        arrCurrentSongPlay = MainActivity.arrCurrentListPlay;
        plaingQueueAdapter = new ListAdapter(getContext(),rcvPlayingQueue,arrCurrentSongPlay);
        rcvPlayingQueue.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvPlayingQueue.setAdapter(plaingQueueAdapter);
    }
}
