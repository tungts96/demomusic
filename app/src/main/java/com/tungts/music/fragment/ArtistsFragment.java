package com.tungts.music.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tungts.music.R;
import com.tungts.music.activity.ActivityDetail;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.interfaces.OnItemRecycleViewClick;
import com.tungts.music.model.Album;
import com.tungts.music.model.Artist;
import com.tungts.music.model.MusicManager;

import java.util.ArrayList;

/**
 * Created by tungts on 7/27/2017.
 */

public class ArtistsFragment extends Fragment {

    View root;
    RecyclerView rcvArtist;
    ListAdapter artistAdapter;
    ArrayList<Artist> arrArtists;
    MusicManager musicManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_artists,container,false);
        musicManager = new MusicManager(getContext());
        innitRecycleView();
        addEvents();
        return root;
    }

    private void addEvents() {
        artistAdapter.setOnItemRecycleViewClick(new OnItemRecycleViewClick() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), ActivityDetail.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("ARTIST",arrArtists.get(position));
                bundle.putInt("TYPE",ActivityDetail.TYPE_ARTIST);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void innitRecycleView() {
        rcvArtist = (RecyclerView) root.findViewById(R.id.rcvArtists);
        arrArtists = (ArrayList<Artist>) musicManager.getListArtists();
        artistAdapter = new ListAdapter(getContext(),rcvArtist,arrArtists);
        rcvArtist.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvArtist.setAdapter(artistAdapter);
    }
}
