package com.tungts.music.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tungts.music.R;
import com.tungts.music.activity.ActivityDetail;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.interfaces.OnClickSongActivityDetail;
import com.tungts.music.interfaces.OnItemRecycleViewClick;
import com.tungts.music.model.Album;
import com.tungts.music.model.Folder;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;

/**
 * Created by tungts on 7/28/2017.
 */

public class DetailAlbumFragment extends Fragment {
    View root;
    Album album;
    Folder folder;
    MusicManager  musicManager;

    RecyclerView rcvSongs;
    ListAdapter adapter;
    ArrayList<Song> songs;
    OnClickSongActivityDetail onClickSong;

    int type;

    public DetailAlbumFragment(int type){
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_detail_album,container,false);
        rcvSongs = (RecyclerView) root.findViewById(R.id.rcvSongs);
        ActivityDetail activityDetail = (ActivityDetail) getActivity();
        musicManager = new MusicManager(getContext());
        if (type == ActivityDetail.TYPE_ALBUM){
            album = activityDetail.getAlbum();
            innitRecycleViewSongs(album);
        } else if (type == ActivityDetail.TYPE_FOLDER){
            folder = activityDetail.getFolder();
            innitRecycleViewSongs(folder);
        }
        addEvents();
        return root;
    }

    private void innitRecycleViewSongs(Folder folder) {
        songs = folder.getListSongs();
        adapter = new ListAdapter(getContext(),rcvSongs, songs);
        rcvSongs.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvSongs.setAdapter(adapter);
    }

    private void innitRecycleViewSongs(Album album){
        songs = new ArrayList<>();
        songs = musicManager.getListSongFromAlbum(album);
        adapter = new ListAdapter(getContext(),rcvSongs, songs);
        rcvSongs.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvSongs.setAdapter(adapter);
    }

    private void addEvents() {
        adapter.setOnItemRecycleViewClick(new OnItemRecycleViewClick() {
            @Override
            public void onItemClick(int position) {
            if (onClickSong != null){
                onClickSong.itemSong(position, songs);
            }
//                Intent intent = new Intent(getContext(),PlayingSongActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putInt("POSITION",position);
//                bundle.putSerializable("ARRSONGPLAYING",arrSongs);
//                intent.putExtras(bundle);
//                startActivity(intent);            L
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onClickSong = (OnClickSongActivityDetail) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onClickSong = null;
    }

}
