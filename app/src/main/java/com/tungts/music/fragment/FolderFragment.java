package com.tungts.music.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tungts.music.R;
import com.tungts.music.activity.ActivityDetail;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.interfaces.OnItemRecycleViewClick;
import com.tungts.music.model.Folder;
import com.tungts.music.model.MusicManager;

import java.util.ArrayList;

/**
 * Created by tungts on 7/27/2017.
 */

public class FolderFragment extends Fragment {

    View root;
    MusicManager musicManager;
    RecyclerView rcvfolder;
    ListAdapter folderAdapter;
    ArrayList<Folder> arrFolders;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_folder,container,false);
        musicManager = new MusicManager(getContext());
        innitRecycleView();
        folderAdapter.setOnItemRecycleViewClick(new OnItemRecycleViewClick() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), ActivityDetail.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("FOLDER",arrFolders.get(position));
                bundle.putInt("TYPE",ActivityDetail.TYPE_FOLDER);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        return root;
    }

    private void innitRecycleView() {
        rcvfolder = (RecyclerView) root.findViewById(R.id.rcvFolder);
        arrFolders = (ArrayList<Folder>) musicManager.getListFolder();
        folderAdapter = new ListAdapter(getContext(),rcvfolder,arrFolders);
        rcvfolder.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvfolder.setAdapter(folderAdapter);
    }
}
