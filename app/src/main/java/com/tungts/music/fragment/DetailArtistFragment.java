package com.tungts.music.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tungts.music.R;
import com.tungts.music.activity.ActivityDetail;
import com.tungts.music.adapter.AlbumOfArtistAlbumAdapter;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.interfaces.OnClickSongActivityDetail;
import com.tungts.music.interfaces.OnItemRecycleViewClick;
import com.tungts.music.model.Album;
import com.tungts.music.model.Artist;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;

/**
 * Created by tungts on 7/28/2017.
 */

public class DetailArtistFragment extends Fragment {

    MusicManager musicManager;
    View view;
    RecyclerView rcvAlbumsOfArtist,rcvSongOfArtist;
    ListAdapter songOfArtistAlbum;ArrayList<Song> songsOfArtist;
    AlbumOfArtistAlbumAdapter albumOfArtistAlbumAdapter;ArrayList<Album> albumOfArtist;
    Artist artist;
    OnClickSongActivityDetail onClickSong;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail_artist,container,false);
        musicManager = new MusicManager(getContext());
        rcvAlbumsOfArtist = (RecyclerView) view.findViewById(R.id.rcvAlbumsOfArtist);
        rcvSongOfArtist = (RecyclerView) view.findViewById(R.id.rcvSongOfArtist);
        ActivityDetail activityDetail = (ActivityDetail) getActivity();
        artist = activityDetail.getArtist();
        innitRecycleViewSongs(artist);
        innitRecycleViewAlbumOfArtist(artist);
        addEvents();
        return view;
    }

    private void addEvents() {
        songOfArtistAlbum.setOnItemRecycleViewClick(new OnItemRecycleViewClick() {
            @Override
            public void onItemClick(int position) {
                onClickSong.itemSong(position,songsOfArtist);
            }
        });

        albumOfArtistAlbumAdapter.setOnItemRecycleViewClick(new OnItemRecycleViewClick() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getContext(), ActivityDetail.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("ALBUM",albumOfArtist.get(position));
                bundle.putInt("TYPE",ActivityDetail.TYPE_ALBUM);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void innitRecycleViewSongs(Artist artist){
        songsOfArtist = new ArrayList<>();
        songsOfArtist = musicManager.getListSongFromArtist(artist);
        songOfArtistAlbum = new ListAdapter(getContext(),rcvSongOfArtist,songsOfArtist);
        rcvSongOfArtist.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvSongOfArtist.setAdapter(songOfArtistAlbum);
    }

    private void innitRecycleViewAlbumOfArtist(Artist artist){
        albumOfArtist = new ArrayList<>();
        albumOfArtist = musicManager.getListAlbumFromArtist(artist);
        albumOfArtistAlbumAdapter = new AlbumOfArtistAlbumAdapter(getContext(),albumOfArtist);
        rcvAlbumsOfArtist.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        rcvAlbumsOfArtist.setAdapter(albumOfArtistAlbumAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onClickSong = (OnClickSongActivityDetail) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onClickSong = null;
    }
}
