package com.tungts.music.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tungts.music.R;
import com.tungts.music.activity.ActivityDetail;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.interfaces.OnItemRecycleViewClick;
import com.tungts.music.model.Album;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;

/**
 * Created by tungts on 7/27/2017.
 */

public class AlbumsFragment extends Fragment {

    View root;
    RecyclerView rcvAlbum;
    ListAdapter albumAdapter;
    ArrayList<Album> arrAlbums;
    MusicManager musicManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_albums,container,false);
        musicManager = new MusicManager(getContext());
        innitRecycleView();
        addEvents();
        return root;
    }

    private void addEvents() {
        albumAdapter.setOnItemRecycleViewClick(new OnItemRecycleViewClick() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(getActivity(), ActivityDetail.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("ALBUM",arrAlbums.get(position));
                bundle.putInt("TYPE",ActivityDetail.TYPE_ALBUM);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void innitRecycleView() {
        rcvAlbum = (RecyclerView) root.findViewById(R.id.rcvAlbums);
        arrAlbums = (ArrayList<Album>) musicManager.getListAlbums();
        albumAdapter = new ListAdapter(getContext(),rcvAlbum,arrAlbums);
        rcvAlbum.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvAlbum.setAdapter(albumAdapter);
    }
}
