package com.tungts.music.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tungts.music.R;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.interfaces.OnClickSongOfListSong;
import com.tungts.music.interfaces.OnItemRecycleViewClick;
import com.tungts.music.model.MediaPlayerManager;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;

/**
 * Created by tungts on 7/27/2017.
 */

public class SongsFragment extends Fragment {

    View root;
    RecyclerView rcvSongs;
    ListAdapter songAdapter;
    ArrayList<Song> arrSongs;
    MusicManager musicManager;
    OnClickSongOfListSong onClickSong;
    MediaPlayerManager mediaPlayerManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_song,container,false);
        musicManager = new MusicManager(getContext());
        innitRecycleview();
        addEvents();
        return root;
    }

    private void addEvents() {
        songAdapter.setOnItemRecycleViewClick(new OnItemRecycleViewClick() {
            @Override
            public void onItemClick(int position) {
                   if (onClickSong != null){
                        onClickSong.itemSongOfMainActivity(position,arrSongs);
                   }
//                Intent intent = new Intent(getContext(),PlayingSongActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putInt("POSITION",position);
//                bundle.putSerializable("ARRSONGPLAYING",arrSongs);
//                intent.putExtras(bundle);
//                startActivity(intent);
            }
        });
    }

    private void innitRecycleview() {
        rcvSongs = (RecyclerView) root.findViewById(R.id.rcvSongs);
        arrSongs = (ArrayList<Song>) musicManager.getListSongsFromStroage();
        songAdapter = new ListAdapter(getContext(),rcvSongs,arrSongs);
        rcvSongs.setLayoutManager(new LinearLayoutManager(getContext()));
        rcvSongs.setAdapter(songAdapter);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            onClickSong = (OnClickSongOfListSong) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onClickSong = null;
    }
}
