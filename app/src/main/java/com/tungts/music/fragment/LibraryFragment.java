package com.tungts.music.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tungts.music.R;
import com.tungts.music.activity.MainActivity;
import com.tungts.music.adapter.ViewPagerAdapter;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;

import static com.tungts.music.CurrentFragment.isFragmentAlbum;
import static com.tungts.music.CurrentFragment.isFragmentArtist;
import static com.tungts.music.CurrentFragment.isFragmentFolder;
import static com.tungts.music.CurrentFragment.isFragmentPlay;
import static com.tungts.music.CurrentFragment.isFragmentSong;
import static com.tungts.music.CurrentFragment.setFragmentCurrent;

/**
 * Created by tungts on 7/26/2017.
 */

public class LibraryFragment extends Fragment {

    View root;
    MainActivity mainActivity;
    Toolbar toolbar;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_library,container,false);
        mainActivity = (MainActivity) getActivity();
//
//        init viewpager
        viewPager = (ViewPager) root.findViewById(R.id.viewPager);
        setUpViewPager();

//        set up tablayout
        tabLayout = (TabLayout) mainActivity.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        return root;
    }

    private void setUpViewPager() {
        viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPagerAdapter.addFragment(new SongsFragment(),"Songs");
        viewPagerAdapter.addFragment(new AlbumsFragment(), "Albums");
        viewPagerAdapter.addFragment(new ArtistsFragment(), "Artists");
        viewPagerAdapter.addFragment(new FolderFragment(), "Folder");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0){
                    setFragmentCurrent(isFragmentSong,true);
                    setFragmentCurrent(isFragmentPlay,false);
                    setFragmentCurrent(isFragmentAlbum,false);
                    setFragmentCurrent(isFragmentArtist,false);
                    setFragmentCurrent(isFragmentFolder,false);
                } else if (position == 1){
                    setFragmentCurrent(isFragmentAlbum,true);
                    setFragmentCurrent(isFragmentPlay,false);
                    setFragmentCurrent(isFragmentSong,false);
                    setFragmentCurrent(isFragmentArtist,false);
                    setFragmentCurrent(isFragmentFolder,false);
                } else if (position == 2){
                    setFragmentCurrent(isFragmentAlbum,false);
                    setFragmentCurrent(isFragmentPlay,false);
                    setFragmentCurrent(isFragmentSong,false);
                    setFragmentCurrent(isFragmentArtist,true);
                    setFragmentCurrent(isFragmentFolder,false);
                } else if (position == 3){
                    setFragmentCurrent(isFragmentAlbum,false);
                    setFragmentCurrent(isFragmentPlay,false);
                    setFragmentCurrent(isFragmentSong,false);
                    setFragmentCurrent(isFragmentArtist,false);
                    setFragmentCurrent(isFragmentFolder,true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}
