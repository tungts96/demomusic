package com.tungts.music;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.tungts.music.activity.MainActivity;
import com.tungts.music.activity.PlayingSongActivity;
import com.tungts.music.interfaces.CurrentItemSong;
import com.tungts.music.model.Song;

import java.util.ArrayList;

import static com.tungts.music.activity.MainActivity.arrCurrentListPlay;
import static com.tungts.music.activity.MainActivity.currentIndexSong;
import static com.tungts.music.activity.MainActivity.mediaPlayerManager;
import static com.tungts.music.activity.PlayingSongActivity.broadcastReceiver;


public class MusicService extends Service{

    MainActivity.MyReceiver myReceiver;
    LocalBroadcastManager localBroadcastManager;

    public MusicService() {
        Log.e("Music", "1");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        myReceiver = new MainActivity.MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.ACTION.UPDATE_ACTION);
        registerReceiver(myReceiver,intentFilter);
        localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        localBroadcastManager.registerReceiver(broadcastReceiver,new IntentFilter(Constants.ACTION.UPDATE_ACTION));
        Log.e("Music", "create");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Music", "start");
//        showNotification();
        if (intent.getAction().equals(Constants.ACTION.STARTFOREGROUND_ACTION)) {
            MediaPlayer mediaPlayer = mediaPlayerManager.getMediaPlayer();
            mediaPlayerManager.setArrPlayListSongs(arrCurrentListPlay);
            mediaPlayerManager.playSong(currentIndexSong);
            showNotification();
            sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
            Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
        } else if (intent.getAction().equals(Constants.ACTION.PREV_ACTION)) {
            previousSong();
            showNotification();
            sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
            localBroadcastManager.sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
            Log.i(LOG_TAG, "Clicked Previous");
        } else if (intent.getAction().equals(Constants.ACTION.PLAY_ACTION)) {
            playPause();
            showNotification();
            sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
            localBroadcastManager.sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
            Log.i(LOG_TAG, "Clicked Play");
        } else if (intent.getAction().equals(Constants.ACTION.PAUSE_ACTION)){
            playPause();
            showNotification();
            sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
            localBroadcastManager.sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
        } else if (intent.getAction().equals(Constants.ACTION.NEXT_ACTION)) {
            nextSong();
            showNotification();
            sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
            localBroadcastManager.sendBroadcast(new Intent(Constants.ACTION.UPDATE_ACTION));
            Log.i(LOG_TAG, "Clicked Next");
        }
        return START_NOT_STICKY;
    }

    public static void playPause(){
        MediaPlayer mediaPlayer = mediaPlayerManager.getMediaPlayer();
        if (mediaPlayer.isPlaying()){
            if (mediaPlayer != null){
                mediaPlayer.pause();
            }
        } else {
            if (mediaPlayer != null){
                mediaPlayer.start();
            }
        }
    }

    public void nextSong(){
        if (currentIndexSong < arrCurrentListPlay.size()-1){
            currentIndexSong++;
        } else {
            currentIndexSong = 0;;
        }
        mediaPlayerManager.playSong(currentIndexSong);
    }

    public void previousSong(){
        if (currentIndexSong > 0){
            currentIndexSong-=1;
        } else {
            currentIndexSong = arrCurrentListPlay.size()-1;;
        }
        mediaPlayerManager.playSong(currentIndexSong);
    }

//    private void addNotification() {
//        MediaPlayer mediaPlayer = MainActivity.mediaPlayerManager.getMediaPlayer();
//        Intent intent = new Intent(this, PlayingSongActivity.class);
//        Bundle bundle = new Bundle();
//        intent.putExtras(bundle);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendInt = PendingIntent.getActivity(this, 0,
//                intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        RemoteViews views = new RemoteViews(getPackageName(), R.layout.custom_notification);
//        views.setTextViewText(R.id.tvNotiName, MainActivity.mediaPlayerManager.getArrPlayListSongs().get(currentIndexSong).getNameSong());
//
//        Intent nextIntent = new Intent(this, MusicService.class);
//        PendingIntent next = PendingIntent.getService(this,0,nextIntent,0);
//        views.setOnClickPendingIntent(R.id.imgNotiNext,next);
//
//        Notification.Builder builder = new Notification.Builder(this);
//            builder.setContentIntent(pendInt)
//                    .setSmallIcon(R.drawable.ic_default)
//                    .setOngoing(true);
////                    .setContentTitle(MainACustomBigctivity.mediaPlayerManager.getArrPlayListSongs().get(MainActivity.currentIndexSong).getNameSong())
////                    .setContentText(MainActivity.mediaPlayerManager.getArrPlayListSongs().get(MainActivity.currentIndexSong).getNameArtist());
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            builder.setCustomContentView(views);
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            not = builder.build();
//        }
//        startForeground(333333, not);
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MediaPlayer mediaPlayer = mediaPlayerManager.getMediaPlayer();
        mediaPlayer.release();
        mediaPlayerManager = null;
        unregisterReceiver(myReceiver);
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);
        Log.e("Music", "Destroy");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    static Notification status;
    private static final String LOG_TAG = "NotificationService";

    private void showNotification() {
    // Using RemoteViews to bind custom layouts into Notification
        RemoteViews views = new RemoteViews(getPackageName(),
                R.layout.custom_notification);

        Intent notificationIntent = new Intent(this, PlayingSongActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent previousIntent = new Intent(this, MusicService.class);
        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
                previousIntent, 0);

        Intent nextIntent = new Intent(this, MusicService.class);
        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                nextIntent, 0);

        if (mediaPlayerManager.getMediaPlayer().isPlaying()){
            views.setImageViewResource(R.id.imgNotiPlayPause,R.drawable.ic_pause_white);
            Intent pauseIntent = new Intent(this, MusicService.class);
            pauseIntent.setAction(Constants.ACTION.PAUSE_ACTION);
            PendingIntent ppauseIntent = PendingIntent.getService(this, 0,
                    pauseIntent, 0);
            views.setOnClickPendingIntent(R.id.imgNotiPlayPause, ppauseIntent);
        } else {
            views.setImageViewResource(R.id.imgNotiPlayPause,R.drawable.ic_play_white);
            Intent playIntent = new Intent(this, MusicService.class);
            playIntent.setAction(Constants.ACTION.PLAY_ACTION);
            PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                    playIntent, 0);
            views.setOnClickPendingIntent(R.id.imgNotiPlayPause, pplayIntent);
        }

        views.setOnClickPendingIntent(R.id.imgNotiNext, pnextIntent);
        views.setOnClickPendingIntent(R.id.imgNotiPre, ppreviousIntent);
        views.setTextViewText(R.id.tvNotiName, arrCurrentListPlay.get(currentIndexSong).getNameSong());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            status = new Notification.Builder(this).build();
            status.contentView = views;
            status.flags = Notification.FLAG_ONGOING_EVENT;
            status.icon = R.drawable.ic_play;
            status.contentIntent = pendingIntent;
        }

        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, status);

    }
}
