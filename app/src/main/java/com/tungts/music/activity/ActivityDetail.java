package com.tungts.music.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tungts.music.Constants;
import com.tungts.music.MusicService;
import com.tungts.music.R;
import com.tungts.music.adapter.AlbumOfArtistAlbumAdapter;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.fragment.DetailAlbumFragment;
import com.tungts.music.fragment.DetailArtistFragment;
import com.tungts.music.interfaces.OnClickSongActivityDetail;
import com.tungts.music.model.Album;
import com.tungts.music.model.Artist;
import com.tungts.music.model.Folder;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;

import static com.tungts.music.activity.MainActivity.arrCurrentListPlay;
import static com.tungts.music.activity.MainActivity.currentIndexSong;
import static com.tungts.music.activity.MainActivity.mediaPlayerManager;

public class ActivityDetail extends AppCompatActivity implements View.OnClickListener ,OnClickSongActivityDetail {

    Bundle bundle;

    Toolbar toolbar;
    CollapsingToolbarLayout collapsingToolbar;
    ImageView imgBackDrop;
    public static int TYPE_ARTIST = 500;
    public static int TYPE_ALBUM = 501;
    public static int TYPE_FOLDER = 502;
    private int type;

    FloatingActionButton floatingActionButton;
    Album album;
    Artist artist;
    Folder folder;

    //bottom sheet
    private View bottomSheet;
    private BottomSheetBehavior bottomSheetBehavior;

    //CardView Song Playing
    private static CardView itemPlaying;
    private static ImageView profileSongPlayingOfCardView;
    private static ImageView imgPlayPauseOfCardView;
    private static TextView tvNameSongPlayingOfCardView;
    private static TextView tvNameArtistOfSongPlayingOfCardView;
    private static ImageView imgNextOfCardView;
    private static ImageView imgPreviousOfCardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        bundle = getIntent().getExtras();
        addToolbar();
        addControls();
        addCollapSingToolbar();
        addBottomSheet();
        if (type == TYPE_ALBUM){
            replaceFragment(new DetailAlbumFragment(TYPE_ALBUM));
        } else if (type == TYPE_ARTIST){
            replaceFragment(new DetailArtistFragment());
        } else if (type == TYPE_FOLDER){
            replaceFragment(new DetailAlbumFragment(TYPE_FOLDER));
        }
        MediaPlayer mediaPlayer = mediaPlayerManager.getMediaPlayer();
        if (mediaPlayer.isPlaying()){
            itemPlaying.setVisibility(View.VISIBLE);
            profileSongPlayingOfCardView.setImageBitmap(arrCurrentListPlay.get(currentIndexSong).getImage());
            tvNameSongPlayingOfCardView.setText(arrCurrentListPlay.get(currentIndexSong).getNameSong());
            tvNameArtistOfSongPlayingOfCardView.setText(arrCurrentListPlay.get(currentIndexSong).getNameArtist());
            imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
        } else {
            imgPlayPauseOfCardView.setImageResource(R.drawable.ic_play);
        }
    }

    private void addBottomSheet() {
        bottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        //card view
        itemPlaying = (CardView) bottomSheet.findViewById(R.id.itemListPlaying);itemPlaying.setOnClickListener(this);
        profileSongPlayingOfCardView = (ImageView) itemPlaying.findViewById(R.id.imglistPlaying);
        tvNameSongPlayingOfCardView = (TextView) itemPlaying.findViewById(R.id.tvList1);
        tvNameArtistOfSongPlayingOfCardView = (TextView) itemPlaying.findViewById(R.id.tvList2);
        imgPlayPauseOfCardView = (ImageView) itemPlaying.findViewById(R.id.imgPlaying);imgPlayPauseOfCardView.setOnClickListener(this);
        imgNextOfCardView = (ImageView) itemPlaying.findViewById(R.id.imgNextPlaying);imgNextOfCardView.setOnClickListener(this);
        imgPreviousOfCardView = (ImageView) itemPlaying.findViewById(R.id.imgPreviousPlaying);imgPreviousOfCardView.setOnClickListener(this);

    }

    private void replaceFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content_main_detail,fragment);
        ft.commit();
    }

    private void addCollapSingToolbar() {
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        imgBackDrop = (ImageView) findViewById(R.id.backdrop);
        imgBackDrop.setImageResource(R.drawable.ic_default_music);
        type = getTypeView();
        if (type == TYPE_ARTIST){
            artist = (Artist) bundle.getSerializable("ARTIST");
            collapsingToolbar.setTitle(artist.getNameArtist());
            floatingActionButton.setVisibility(View.INVISIBLE);
        } else if (type == TYPE_ALBUM){
            album = (Album) bundle.getSerializable("ALBUM");
            collapsingToolbar.setTitle(album.getNameAlbum());
            if (album.getImageAlbum() != null){
                imgBackDrop.setImageBitmap(album.getImageAlbum());
            }
        } else if (type == TYPE_FOLDER){
            folder = (Folder) bundle.getSerializable("FOLDER");
            imgBackDrop.setImageResource(R.drawable.ic_default);
            collapsingToolbar.setTitle(folder.getNameFolder());
        }
    }

    private void addControls() {
        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
    }

    private void addToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public int getTypeView(){
        return bundle.getInt("TYPE");
    }

    public Album getAlbum(){
        return (Album) bundle.getSerializable("ALBUM");
    }

    public Artist getArtist(){
        return (Artist) bundle.getSerializable("ARTIST");
    }

    public Folder getFolder() {
        return (Folder) bundle.getSerializable("FOLDER");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        } else if (item.getItemId() == R.id.action_search){
            Intent intent = new Intent(ActivityDetail.this,ActivitySearch.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setPlayOrPauseButton() throws Exception{
        MediaPlayer mediaPlayer = mediaPlayerManager.getMediaPlayer();
        if (mediaPlayer.isPlaying()){
            if (mediaPlayer != null){
                imgPlayPauseOfCardView.setImageResource(R.drawable.ic_play);
                Intent myIntent = new Intent(ActivityDetail.this, MusicService.class);
                myIntent.setAction(Constants.ACTION.PAUSE_ACTION);
                this.startService(myIntent);
            }
        } else {
            if (mediaPlayer != null){
                imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
                Intent myIntent = new Intent(ActivityDetail.this, MusicService.class);
                myIntent.setAction(Constants.ACTION.PLAY_ACTION);
                this.startService(myIntent);
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.imgPlaying:
                try {
                    setPlayOrPauseButton();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.imgNextPlaying:
                startServices(Constants.ACTION.NEXT_ACTION);
                break;
            case R.id.imgPreviousPlaying:
                startServices(Constants.ACTION.PREV_ACTION);
                break;
            case R.id.itemListPlaying:
                Intent intent = new Intent(ActivityDetail.this,PlayingSongActivity.class);
                startActivity(intent);
                break;
        }

    }

    @Override
    public void itemSong(int positon, ArrayList<Song> arrSongs) {
        Song song = arrSongs.get(positon);
        arrCurrentListPlay = arrSongs;
        currentIndexSong = positon;
        setViewForCurrentSong(song);
        imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
        itemPlaying.setVisibility(View.VISIBLE);
        startServices(Constants.ACTION.STARTFOREGROUND_ACTION);
    }

    private void startServices(String action) {
        Intent myIntent = new Intent(ActivityDetail.this, MusicService.class);
        myIntent.setAction(action);
        this.startService(myIntent);
    }

    public void setViewForCurrentSong(Song song) {
        //card view bottom sheet
        profileSongPlayingOfCardView.setImageBitmap(song.getImage());
        tvNameSongPlayingOfCardView.setText(song.getNameSong());
        tvNameArtistOfSongPlayingOfCardView.setText(song.getNameArtist());
    }

    public static BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION.UPDATE_ACTION)){
                    itemPlaying.setVisibility(View.VISIBLE);
                    if (mediaPlayerManager.getMediaPlayer().isPlaying()){
                        imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
                    } else {
                        imgPlayPauseOfCardView.setImageResource(R.drawable.ic_play);
                    }

                    Song song = arrCurrentListPlay.get(currentIndexSong);
                    //card view bottom sheet
                    profileSongPlayingOfCardView.setImageBitmap(song.getImage());
                    tvNameSongPlayingOfCardView.setText(song.getNameSong());
                    tvNameArtistOfSongPlayingOfCardView.setText(song.getNameArtist());
            }
        }
    };
}
