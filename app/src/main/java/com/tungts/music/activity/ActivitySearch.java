package com.tungts.music.activity;

import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.tungts.music.R;
import com.tungts.music.adapter.ListAdapter;
import com.tungts.music.model.Album;
import com.tungts.music.model.Artist;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;
import java.util.List;

public class ActivitySearch extends AppCompatActivity implements SearchView.OnQueryTextListener{

    Toolbar toolbar;
    SearchView searchView;

    RecyclerView rcvSearch;
    ListAdapter adapter;
    ArrayList arr;

    ArrayList<Song> arrSongs;
    ArrayList<Album> arrAlbums;
    ArrayList<Artist> arrArtists;

    MusicManager musicManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        musicManager = new MusicManager(this);
        arrSongs = (ArrayList<Song>) musicManager.getListSongsFromStroage();
        arrAlbums = (ArrayList<Album>) musicManager.getListAlbums();
        arrArtists = (ArrayList<Artist>) musicManager.getListArtists();
        addActionBar();
        addSearchView();
        addRecycleView();
    }

    private void addRecycleView() {
        rcvSearch = (RecyclerView) findViewById(R.id.rcvSearch);
        arr = new ArrayList();
        adapter = new ListAdapter(this,rcvSearch,arr);
        rcvSearch.setLayoutManager(new LinearLayoutManager(this));
        rcvSearch.setAdapter(adapter);
    }

    private void addSearchView() {
        searchView = (SearchView) toolbar.findViewById(R.id.searchView);
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint("Search Library");
        searchView.setOnQueryTextListener(this);
    }

    private void addActionBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void search(String query) {
        for (Song song:arrSongs){
            if (song.getNameSong().contains(query)){
                arr.add(song);
            }
        }
        if (arr.size() != 0){
            arr.add(0,"Songs");
        }
        int size = arr.size();
        for (Album album:arrAlbums){
            if (album.getNameAlbum().contains(query)){
                arr.add(album);
            }
        }
        if (arr.size() > size){
            arr.add(size,"Albums");
            size = arr.size();
        }
        for (Artist artist:arrArtists){
            if (artist.getNameArtist().contains(query)){
                arr.add(artist);
            }
        }
        if (arr.size() > size){
            arr.add(size,"Artist");
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        arr.clear();
        adapter.notifyDataSetChanged();
        if (query.length()>0) search(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        arr.clear();
        adapter.notifyDataSetChanged();
        if (newText.length()>0) search(newText);
        return false;
    }

}
