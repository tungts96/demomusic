package com.tungts.music.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.tungts.music.Constants;
import com.tungts.music.MusicService;
import com.tungts.music.R;
import com.tungts.music.interfaces.CurrentItemSong;
import com.tungts.music.model.Song;

import java.util.ArrayList;

import static com.tungts.music.activity.MainActivity.mediaPlayerManager;

public class PlayingSongActivity extends AppCompatActivity implements View.OnClickListener,SeekBar.OnSeekBarChangeListener,CurrentItemSong {

    private int currentPosition;

    private ImageView imgNext;
    private ImageView imgPrevious;
    private static ImageView imgPlayPause;
    private static ImageView imgSongProfile;
    private SeekBar songProgressBar;
    private static TextView tvNameSong;
    private static TextView tvNameArtist;
    MediaPlayer mediaPlayer;

    boolean musicBound;
    Intent playIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing_song);
        addControls();
        addEvents();
        mediaPlayer = mediaPlayerManager.getMediaPlayer();
        if (MainActivity.arrCurrentListPlay.get(MainActivity.currentIndexSong).getImage() != null){
            imgSongProfile.setImageBitmap(MainActivity.arrCurrentListPlay.get(MainActivity.currentIndexSong).getImage());
        } else {
            imgSongProfile.setImageResource(R.drawable.ic_disk);
        }
        tvNameArtist.setText(MainActivity.arrCurrentListPlay.get(MainActivity.currentIndexSong).getNameArtist());
        tvNameSong.setText(MainActivity.arrCurrentListPlay.get(MainActivity.currentIndexSong).getNameSong());

        if (mediaPlayer.isPlaying()){
            int currentDuration = mediaPlayer.getCurrentPosition();
            int totalduration = mediaPlayer.getDuration();
            int progress = (int) (((double) currentDuration / totalduration) * 100);
            songProgressBar.setProgress(progress);
            updateSongProgressBar();
            imgPlayPause.setImageResource(R.drawable.ic_pause_white);
            imgSongProfile.startAnimation(getAnimation());
        } else {
            imgPlayPause.setImageResource(R.drawable.ic_play_white);
            imgSongProfile.setAnimation(null);
        }

    }

    public static BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION.UPDATE_ACTION)){
                try {
                    if (mediaPlayerManager.getMediaPlayer().isPlaying()){
                        imgPlayPause.setImageResource(R.drawable.ic_pause_white);
                        imgSongProfile.startAnimation(getAnimation());
                    } else {
                        imgPlayPause.setImageResource(R.drawable.ic_play_white);
                        imgSongProfile.setAnimation(null);
                    }
                    tvNameArtist.setText(MainActivity.arrCurrentListPlay.get(MainActivity.currentIndexSong).getNameArtist());
                    tvNameSong.setText(MainActivity.arrCurrentListPlay.get(MainActivity.currentIndexSong).getNameSong());
                    if (MainActivity.arrCurrentListPlay.get(MainActivity.currentIndexSong).getImage() != null){
                        imgSongProfile.setImageBitmap(MainActivity.arrCurrentListPlay.get(MainActivity.currentIndexSong).getImage());
                    } else {
                        imgSongProfile.setImageResource(R.drawable.ic_disk);
                    }
                } catch (Exception e){

                }
            }
        }
    };



    Handler handler = new Handler();

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try{
                long currentDuration = mediaPlayer.getCurrentPosition();
                long duration = mediaPlayer.getDuration();
                double progress = ((double) currentDuration / duration) * 100;
                songProgressBar.setProgress((int) progress);
                handler.postDelayed(this, 100);
            } catch (Exception e){

            }
        }
    };

    private void updateSongProgressBar() {
        handler.postDelayed(runnable,100);
    }

    private void addControls() {
        imgSongProfile = (ImageView) findViewById(R.id.imgSongProfile);
        imgNext = (ImageView) findViewById(R.id.imgNextSong);
        imgPrevious = (ImageView) findViewById(R.id.imgPreviousSong);
        imgPlayPause = (ImageView) findViewById(R.id.imgPlayPause);
        songProgressBar = (SeekBar) findViewById(R.id.seekBar);songProgressBar.setMax(100);
        tvNameSong = (TextView) findViewById(R.id.tvNameMusicIsPlaying);
        tvNameArtist = (TextView) findViewById(R.id.tvArtistMusicIsPlaying);
    }

    private void addEvents(){
        songProgressBar.setOnSeekBarChangeListener(this);
        imgPlayPause.setOnClickListener(this);
        imgPrevious.setOnClickListener(this);
        imgNext.setOnClickListener(this);
        MainActivity.mediaPlayerManager.setOnCompetionMediaPlayer(this);
    }

    private  void startService(String action){
        Intent myIntent = new Intent(PlayingSongActivity.this, MusicService.class);
        myIntent.setAction(action);
        this.startService(myIntent);
    }

    private void stopServices(){
        Intent myIntent = new Intent(PlayingSongActivity.this, MusicService.class);
        this.stopService(myIntent);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.imgNextSong:
                startService(Constants.ACTION.NEXT_ACTION);
                break;
            case R.id.imgPreviousSong:
                startService(Constants.ACTION.PREV_ACTION);
                break;
            case R.id.imgPlayPause:
                setPlayPauseButton();
                break;
        }
    }

    private void setPlayPauseButton() {
        if (mediaPlayerManager.getMediaPlayer().isPlaying()){
            startService(Constants.ACTION.PAUSE_ACTION);
            imgPlayPause.setImageResource(R.drawable.ic_play_white);
        } else {
            startService(Constants.ACTION.PLAY_ACTION);
            imgPlayPause.setImageResource(R.drawable.ic_pause_white);
        }
    }

    private static RotateAnimation getAnimation(){
        int durationMillis = 20000;
        RotateAnimation r = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF,0.5f);
        r.setFillEnabled(true);
        r.setFillAfter(false);
        r.setDuration(durationMillis);
        r.setInterpolator(new LinearInterpolator());
        r.setRepeatMode(Animation.RESTART);
        r.setRepeatCount(Animation.INFINITE);
        return r;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("Play","Stop");
    }

    @Override
    protected void onDestroy() {
        Log.e("Play","Des");
        super.onDestroy();
//        mediaPlayer.release();
    }

    //seekBar
    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        MediaPlayer mediaPlayer = mediaPlayerManager.getMediaPlayer();
        handler.removeCallbacks(runnable);
        int progress = seekBar.getProgress();
        int currentDuration = 0;
        int totalduration = mediaPlayer.getDuration();
        currentDuration = (int) ((((double) progress) / 100) * totalduration);
        mediaPlayer.seekTo(currentDuration);
        updateSongProgressBar();
    }

    @Override
    public void currentSong(Song song, int pos, ArrayList<Song> songs) {
//        MainActivity.currentIndexSong = pos;
//        MainActivity.arrCurrentListPlay = songs;
        tvNameArtist.setText(songs.get(pos).getNameArtist());
        tvNameSong.setText(songs.get(pos).getNameSong());
        startService(Constants.ACTION.NEXT_ACTION);
    }

}
