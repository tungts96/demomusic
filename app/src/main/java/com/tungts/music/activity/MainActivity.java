package com.tungts.music.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.tungts.music.Constants;
import com.tungts.music.MusicService;
import com.tungts.music.R;
import com.tungts.music.adapter.ViewPagerAdapter;
import com.tungts.music.fragment.LibraryFragment;
import com.tungts.music.fragment.PlayingQueueFragment;
import com.tungts.music.fragment.PlaylistsFragment;
import com.tungts.music.interfaces.OnClickSongOfListSong;
import com.tungts.music.interfaces.CurrentItemSong;
import com.tungts.music.model.MediaPlayerManager;
import com.tungts.music.model.MusicManager;
import com.tungts.music.model.Song;

import java.util.ArrayList;

import static com.tungts.music.CurrentFragment.isFragmentAlbum;
import static com.tungts.music.CurrentFragment.isFragmentArtist;
import static com.tungts.music.CurrentFragment.isFragmentFolder;
import static com.tungts.music.CurrentFragment.isFragmentPlay;
import static com.tungts.music.CurrentFragment.isFragmentSong;
import static com.tungts.music.CurrentFragment.setFragmentCurrent;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnClickSongOfListSong, View.OnClickListener ,CurrentItemSong{

    //bottom sheet
    private View bottomSheet;
    private BottomSheetBehavior bottomSheetBehavior;
    private static RelativeLayout rtlBottomSheet;
    private static ImageView imgPlayPauseOfBottomSheet;
    private static TextView tvNameSongplayingOfBottomSheet;
    private static TextView tvArtistSongplayingOfBottomSheet;
    private SeekBar seekBar;

    //CardView Song Playing
    private static CardView itemPlaying;
    private static ImageView profileSongPlayingOfCardView;
    private static ImageView imgPlayPauseOfCardView;
    private static TextView tvNameSongPlayingOfCardView;
    private static TextView tvNameArtistOfSongPlayingOfCardView;
    private static ImageView imgNextOfCardView;
    private static ImageView imgPreviousOfCardView;


    //navi header
    private static TextView tvNaviNameSong;
    private static TextView tvNaviArtistSong;
    private static RelativeLayout rtlNaviHeader;

    //MediaplayerManager
    public static MediaPlayerManager mediaPlayerManager;
    static ImageView imgNextSong,imgPreviousSong;
    public static ArrayList<Song> arrCurrentListPlay;
    public static int currentIndexSong;

    //viewpager
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //add actionbar and drawer
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //navigation view
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        rtlNaviHeader = (RelativeLayout) navigationView.getHeaderView(0);
        tvNaviNameSong = (TextView) rtlNaviHeader.findViewById(R.id.tvNaviNameSong);
        tvNaviArtistSong = (TextView) rtlNaviHeader.findViewById(R.id.tvNaviArtistSong);

//        //init viewpager
//        viewPager = (ViewPager) findViewById(R.id.viewPager);
//        setUpViewPager();

        //set up tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
//        tabLayout.setupWithViewPager(viewPager);
        replaceFragment(new LibraryFragment());
        addBottomSheet();

//        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) drawer.findViewById(R.id.coordinatorLayout);
//        AppBarLayout appBarLayout = (AppBarLayout) coordinatorLayout.findViewById(R.id.app_bar_layout);
//        TabLayout tabLayout = (TabLayout) appBarLayout.findViewById(R.id.tabLayout);
//        tabLayout.setVisibility(View.GONE);
    }

    public ArrayList<Song> getArrCurrentListPlay(){
        return arrCurrentListPlay;
    }

//    private void setUpViewPager() {
//        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        viewPagerAdapter.addFragment(new SongsFragment(),"Songs");
//        viewPagerAdapter.addFragment(new AlbumsFragment(), "Albums");
//        viewPagerAdapter.addFragment(new ArtistsFragment(), "Artists");
//        viewPagerAdapter.addFragment(new FolderFragment(), "Folder");
//        viewPager.setAdapter(viewPagerAdapter);
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                if (position == 0){
//                    setFragmentCurrent(isFragmentSong,true);
//                    setFragmentCurrent(isFragmentPlay,false);
//                    setFragmentCurrent(isFragmentAlbum,false);
//                    setFragmentCurrent(isFragmentArtist,false);
//                    setFragmentCurrent(isFragmentFolder,false);
//                } else if (position == 1){
//                    setFragmentCurrent(isFragmentAlbum,true);
//                    setFragmentCurrent(isFragmentPlay,false);
//                    setFragmentCurrent(isFragmentSong,false);
//                    setFragmentCurrent(isFragmentArtist,false);
//                    setFragmentCurrent(isFragmentFolder,false);
//                } else if (position == 2){
//                    setFragmentCurrent(isFragmentAlbum,false);
//                    setFragmentCurrent(isFragmentPlay,false);
//                    setFragmentCurrent(isFragmentSong,false);
//                    setFragmentCurrent(isFragmentArtist,true);
//                    setFragmentCurrent(isFragmentFolder,false);
//                } else if (position == 3){
//                    setFragmentCurrent(isFragmentAlbum,false);
//                    setFragmentCurrent(isFragmentPlay,false);
//                    setFragmentCurrent(isFragmentSong,false);
//                    setFragmentCurrent(isFragmentArtist,false);
//                    setFragmentCurrent(isFragmentFolder,true);
//                }
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//    }

    private void addMediaPlayerManager() {
        MusicManager musicManager = new MusicManager(this);
        arrCurrentListPlay = (ArrayList<Song>) musicManager.getListSongsFromStroage();
        mediaPlayerManager = new MediaPlayerManager();
        mediaPlayerManager.setArrPlayListSongs(arrCurrentListPlay);
        mediaPlayerManager.setOnCompetionMediaPlayer(this);
    }

    private void addBottomSheet() {
        bottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
////        layout bottom Sheet
//        rtlBottomSheet = (RelativeLayout) bottomSheet.findViewById(R.id.rltBottomSheet);
//        imgNextSong = (ImageView) rtlBottomSheet.findViewById(R.id.imgNextSong);imgNextSong.setOnClickListener(this);
//        imgPreviousSong = (ImageView) rtlBottomSheet.findViewById(R.id.imgPreviousSong);imgPreviousSong.setOnClickListener(this);
//        tvNameSongplayingOfBottomSheet = (TextView) rtlBottomSheet.findViewById(R.id.tvNameMusicIsPlaying);
//        tvArtistSongplayingOfBottomSheet = (TextView) rtlBottomSheet.findViewById(R.id.tvArtistMusicIsPlaying);
//        imgPlayPauseOfBottomSheet = (ImageView) rtlBottomSheet.findViewById(R.id.imgPlayPause);imgPlayPauseOfBottomSheet.setOnClickListener(this);
//        seekBar = (SeekBar) rtlBottomSheet.findViewById(R.id.seekBar);

        //layout cardview Of Bottom Sheet
        itemPlaying = (CardView) bottomSheet.findViewById(R.id.itemListPlaying);itemPlaying.setOnClickListener(this);
        profileSongPlayingOfCardView = (ImageView) itemPlaying.findViewById(R.id.imglistPlaying);
        tvNameSongPlayingOfCardView = (TextView) itemPlaying.findViewById(R.id.tvList1);
        tvNameArtistOfSongPlayingOfCardView = (TextView) itemPlaying.findViewById(R.id.tvList2);
        imgPlayPauseOfCardView = (ImageView) itemPlaying.findViewById(R.id.imgPlaying);imgPlayPauseOfCardView.setOnClickListener(this);
        imgNextOfCardView = (ImageView) itemPlaying.findViewById(R.id.imgNextPlaying);imgNextOfCardView.setOnClickListener(this);
        imgPreviousOfCardView = (ImageView) itemPlaying.findViewById(R.id.imgPreviousPlaying);imgPreviousOfCardView.setOnClickListener(this);

        addMediaPlayerManager();
    }

    private void replaceFragment(Fragment fragment) {
        if (fragment != null){
            FragmentManager fr = getSupportFragmentManager();
            FragmentTransaction ft = fr.beginTransaction();
            ft.replace(R.id.content_main,fragment);
            ft.commit();
            isFragmentSong = true;
        }
    }


//    private void addFragment(Fragment fragment,String tag){
//        if (fragment != null){
//            FragmentManager fr = getSupportFragmentManager();
//            FragmentTransaction ft = fr.beginTransaction();
//            ft.add(R.id.content_main,fragment);
//            ft.addToBackStack(tag);
//            ft.commit();
//        }
//    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if(bottomSheetBehavior.getState() == bottomSheetBehavior.STATE_EXPANDED){
            bottomSheetBehavior.setState(bottomSheetBehavior.STATE_COLLAPSED);
        } else {
            Intent setIntent = new Intent(Intent.ACTION_MAIN);
            setIntent.addCategory(Intent.CATEGORY_HOME);
            setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(setIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search){
            Intent intent = new Intent(MainActivity.this,ActivitySearch.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.action_more){
            menuMore();
        }
        return super.onOptionsItemSelected(item);
    }

    //poup menu more
    private void menuMore() {
        if (isFragmentSong){
        } else if (isFragmentAlbum){
        } else if (isFragmentArtist){
        } else if (isFragmentFolder){
        } else if (isFragmentPlay){
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        switch (item.getItemId()){
            case R.id.nav_Library:
                tabLayout.setVisibility(View.VISIBLE);
                drawer.closeDrawer(GravityCompat.START);
                replaceFragment(new LibraryFragment());
                break;
            case R.id.nav_Playlists:
                tabLayout.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                replaceFragment(new PlaylistsFragment());
                setFragmentCurrent(isFragmentPlay,true);
                setFragmentCurrent(isFragmentSong,false);
                setFragmentCurrent(isFragmentAlbum,false);
                setFragmentCurrent(isFragmentArtist,false);
                setFragmentCurrent(isFragmentFolder,false);
                break;
            case R.id.nav_PlayingQueue:
                tabLayout.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                replaceFragment(new PlayingQueueFragment());
                break;
            case R.id.nav_NowPlaying:
                tabLayout.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                startActivity(new Intent(MainActivity.this,PlayingSongActivity.class));
                break;
            case R.id.nav_Settings:
                tabLayout.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                showToast("Settings");
                break;
            case R.id.nav_Help:
                tabLayout.setVisibility(View.GONE);
                drawer.closeDrawer(GravityCompat.START);
                showToast("Help");
                break;
        }
        return true;
    }


    //when click song
    @Override
    public void itemSongOfMainActivity(int positon, ArrayList<Song> arrSongs) {
        Song song = arrSongs.get(positon);
        arrCurrentListPlay = arrSongs;
        currentIndexSong = positon;
        setViewForCurrentSong(song);

        imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
//        imgPlayPauseOfBottomSheet.setImageResource(R.drawable.ic_pause_white);

        //MediaplayerManager
//        if (mediaPlayerManager.getMediaPlayer().isPlaying()){
//            if(mediaPlayerManager.getMediaPlayer() != null){
//                mediaPlayerManager.getMediaPlayer().release();
//            };
//        }
//        mediaPlayerManager.setArrPlayListSongs(arrSongs);
//        mediaPlayerManager.playSong(positon)
        itemPlaying.setVisibility(View.VISIBLE);
        startServices(Constants.ACTION.STARTFOREGROUND_ACTION);
    }

    public void setViewForCurrentSong(Song song){
        //card view bottom sheet
        profileSongPlayingOfCardView.setImageBitmap(song.getImage());
        tvNameSongPlayingOfCardView.setText(song.getNameSong());
        tvNameArtistOfSongPlayingOfCardView.setText(song.getNameArtist());

        //rtl Bottomsheet
        if (song.getImage() != null){
//            rtlBottomSheet.setBackgroundDrawable(new BitmapDrawable(song.getImage()));
            rtlNaviHeader.setBackgroundDrawable(new BitmapDrawable(song.getImage()));
        } else {
//            rtlBottomSheet.setBackgroundResource(R.drawable.ic_default_music);
            rtlNaviHeader.setBackgroundResource(R.drawable.ic_default_music);
        }
//        tvNameSongplayingOfBottomSheet.setText(song.getNameSong());
//        tvArtistSongplayingOfBottomSheet.setText(song.getNameArtist());

        //navi header
        tvNaviArtistSong.setText(song.getNameArtist());
        tvNaviNameSong.setText(song.getNameSong());
    }

    //clik play pause
    private void setPlayOrPauseButton() throws Exception{
        MediaPlayer mediaPlayer = mediaPlayerManager.getMediaPlayer();
        if (mediaPlayer.isPlaying()){
            if (mediaPlayer != null){
//                mediaPlayer.pause();
                imgPlayPauseOfCardView.setImageResource(R.drawable.ic_play);
//                imgPlayPauseOfBottomSheet.setImageResource(R.drawable.ic_play_white);
                Intent myIntent = new Intent(MainActivity.this, MusicService.class);
                myIntent.setAction(Constants.ACTION.PAUSE_ACTION);
                this.startService(myIntent);;
            }
        } else {
            if (mediaPlayer != null){
//                mediaPlayer.start();
//                imgPlayPauseOfBottomSheet.setImageResource(R.drawable.ic_pause_white);
                imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
                Intent myIntent = new Intent(MainActivity.this, MusicService.class);
                myIntent.setAction(Constants.ACTION.PLAY_ACTION);
                this.startService(myIntent);;
            }
        }
    }

    private void startServices(String action){
        Intent myIntent = new Intent(MainActivity.this, MusicService.class);
        myIntent.setAction(action);
        this.startService(myIntent);
    }

    private void stopServices(){
        Intent myIntent = new Intent(MainActivity.this, MusicService.class);
        this.stopService(myIntent);
    }

    @Override
    public void onClick(View view) {
        int id  = view.getId();
            switch (id){
                //next
                case R.id.imgNextSong:
                    startServices(Constants.ACTION.NEXT_ACTION);
                    setViewForCurrentSong(arrCurrentListPlay.get(currentIndexSong));
//                    imgPlayPauseOfBottomSheet.setImageResource(R.drawable.ic_pause_white);
                    imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
                    break;
                //previous
                case R.id.imgPreviousSong:
//                    mediaPlayerManager.previousSong();
                    startServices(Constants.ACTION.PREV_ACTION);
                    setViewForCurrentSong(arrCurrentListPlay.get(currentIndexSong));
//                    imgPlayPauseOfBottomSheet.setImageResource(R.drawable.ic_pause_white);
                    imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
                    break;
                case R.id.imgPlayPause:
                    try {
                        setPlayOrPauseButton();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.imgPlaying:
                    try {
                        setPlayOrPauseButton();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case R.id.imgNextPlaying:
                    startServices(Constants.ACTION.NEXT_ACTION);
                    break;
                case R.id.imgPreviousPlaying:
                    startServices(Constants.ACTION.PREV_ACTION);
                    break;
                case R.id.itemListPlaying:
                    Intent intent = new Intent(MainActivity.this,PlayingSongActivity.class);
                    startActivity(intent);
                    break;
            }
    }

    @Override
    public void currentSong(Song song, int pos,ArrayList<Song> songs) {
        setViewForCurrentSong(songs.get(pos));
        startServices(Constants.ACTION.NEXT_ACTION);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayerManager.getMediaPlayer().release();
        stopServices();
    }

    //    private ServiceConnection serviceConnection = new ServiceConnection() {
//        @Override
//        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
//            MediaPlayerManager.MusicBinder binder = (MediaPlayerManager.MusicBinder) iBinder;
//            //getservice
//            mediaPlayerManager = binder.getService();
//            musicBound = true;
//        }
//
//        @Override
//        public void onServiceDisconnected(ComponentName componentName) {
//            musicBound = false;
//        }
//    };
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        if(playIntent==null){
//            playIntent = new Intent(this, MediaPlayerManager.class);
//            bindService(playIntent, serviceConnection, this.BIND_AUTO_CREATE);
////            startService(playIntent);
//        }
//    }
//
//    @Override
//    protected void onDestroy() {
//        Log.e("Music","accdes");
//        unbindService(serviceConnection);
////        stopService(playIntent);
//        mediaPlayerManager=null;
//        super.onDestroy();
//    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public static class MyReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Constants.ACTION.UPDATE_ACTION)){
                itemPlaying.setVisibility(View.VISIBLE);
                if (mediaPlayerManager.getMediaPlayer().isPlaying()){
//                    imgPlayPauseOfBottomSheet.setImageResource(R.drawable.ic_pause_white);
                    imgPlayPauseOfCardView.setImageResource(R.drawable.ic_pause);
                } else {
//                    imgPlayPauseOfBottomSheet.setImageResource(R.drawable.ic_play_white);
                    imgPlayPauseOfCardView.setImageResource(R.drawable.ic_play);
                }

                Song song = arrCurrentListPlay.get(currentIndexSong);
                //card view bottom sheet
                profileSongPlayingOfCardView.setImageBitmap(song.getImage());
                tvNameSongPlayingOfCardView.setText(song.getNameSong());
                tvNameArtistOfSongPlayingOfCardView.setText(song.getNameArtist());

//                //rtl Bottomsheet
//                if (song.getImage() != null){
//                    rtlBottomSheet.setBackgroundDrawable(new BitmapDrawable(song.getImage()));
//                    rtlNaviHeader.setBackgroundDrawable(new BitmapDrawable(song.getImage()));
//                } else {
//                    rtlBottomSheet.setBackgroundResource(R.drawable.ic_default_music);
//                    rtlNaviHeader.setBackgroundResource(R.drawable.ic_default_music);
//                }
//                tvNameSongplayingOfBottomSheet.setText(song.getNameSong());
//                tvArtistSongplayingOfBottomSheet.setText(song.getNameArtist());

                //navi header
                tvNaviArtistSong.setText(song.getNameArtist());
                tvNaviNameSong.setText(song.getNameSong());
            }
        }
    }

//    void checkReadPermission() {
//        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(
//                    getActivity(),
//                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                    111);
//            return;
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == 111) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                checkWritePermission();
//            }
//        }
//
//        if (requestCode == 222) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                menuUpdateAvatar();
//            }
//        }
//
//        if(requestCode == 333){
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                getImageFromCamera();
//            }
//        }
//    }
}
