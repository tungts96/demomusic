package com.tungts.music.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.tungts.music.R;
import com.tungts.music.interfaces.OnItemRecycleViewClick;
import com.tungts.music.model.Album;

import java.util.List;

/**
 * Created by tungts on 7/28/2017.
 */

public class AlbumOfArtistAlbumAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    static int ITEM_ALBUM_OF_ARTIST = 1000;
    private Context context;
    private List list;
    LayoutInflater layoutInflater;
    private OnItemRecycleViewClick onItemRecycleViewClick;

    public AlbumOfArtistAlbumAdapter(Context context, List list) {
        this.context = context;
        this.list = list;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_ALBUM_OF_ARTIST){
            View view = layoutInflater.inflate(R.layout.item_album_of_artist,parent,false);
            return new AlbumOfArtistViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AlbumOfArtistViewHolder){
            Album album = (Album) list.get(position);
            if (album.getImageAlbum() != null){
                ((AlbumOfArtistViewHolder) holder).img.setImageBitmap(album.getImageAlbum());
            }
            ((AlbumOfArtistViewHolder) holder).tvName.setText(album.getNameAlbum());
            ((AlbumOfArtistViewHolder) holder).tvNumberOfSong.setText(album.getNumberOfSong()+" songs");
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) instanceof Album){
            return ITEM_ALBUM_OF_ARTIST;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnItemRecycleViewClick(OnItemRecycleViewClick onItemRecycleViewClick){
        this.onItemRecycleViewClick = onItemRecycleViewClick;
    }

    class AlbumOfArtistViewHolder extends RecyclerView.ViewHolder{

        ImageView img;
        TextView tvName,tvNumberOfSong;

        public AlbumOfArtistViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.imgAlbumOfArtist);
            tvName = (TextView) itemView.findViewById(R.id.tvAlbumOfArtist);
            tvNumberOfSong = (TextView) itemView.findViewById(R.id.tvNumberSongsOfAlbum);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemRecycleViewClick != null){
                        onItemRecycleViewClick.onItemClick(getAdapterPosition());
                    }
                }
            });
        }
    }
}
