package com.tungts.music.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tungts.music.R;
import com.tungts.music.interfaces.OnItemRecycleViewClick;
import com.tungts.music.model.Album;
import com.tungts.music.model.Artist;
import com.tungts.music.model.Folder;
import com.tungts.music.model.Song;

import java.util.List;

/**
 * Created by tungts on 7/27/2017.
 */

public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public static final int ITEM_SONG = 98;
    public static final int ITEM_AlBUM = 99;
    public static final int ITEM_ARTIST = 100;
    public static final int ITEM_FOLDER = 101;
    public static final int ITEM_TEXT = 102;

    private OnItemRecycleViewClick onItemRecycleViewClick;
    private Context context;
    private RecyclerView recyclerView;
    private List listData;
    LayoutInflater layoutInflater;

    public ListAdapter(Context context, RecyclerView recyclerView, List listData) {
        this.context = context;
        this.recyclerView = recyclerView;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_list,parent,false);
        if (viewType == ITEM_SONG){
            return new SongViewHolder(view);
        } else if (viewType == ITEM_AlBUM){
            return new AlbumViewHolder(view);
        } else if (viewType == ITEM_ARTIST){
            return new ArtistViewHolder(view);
        } else if (viewType == ITEM_FOLDER){
            return new FolderViewHolder(view);
        } else if (viewType == ITEM_TEXT){
            return new TextViewHolder(layoutInflater.inflate(R.layout.item_text,parent,false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SongViewHolder){
            Song song = (Song) listData.get(position);
            ((SongViewHolder) holder).tvName.setText(song.getNameSong());
            ((SongViewHolder) holder).tvArtist.setText(song.getNameArtist());
            ((SongViewHolder) holder).imgSong.setImageBitmap(song.getImage());
        } else if (holder instanceof AlbumViewHolder){
            Album album = (Album) listData.get(position);
            ((AlbumViewHolder) holder).tvArtist.setText(album.getNameArtist());
            ((AlbumViewHolder) holder).tvName.setText(album.getNameAlbum());
            ((AlbumViewHolder) holder).imgAlbum.setImageBitmap(album.getImageAlbum());
        } else if (holder instanceof ArtistViewHolder){
            Artist artist = (Artist) listData.get(position);
            ((ArtistViewHolder) holder).tvName.setText(artist.getNameArtist());
            ((ArtistViewHolder) holder).tvNumberAlbumAndTrack.setText(context.getString(R.string.number_album_track_of_artist,artist.getNumberOfAlbums(),artist.getNumberOfTracks()));
            ((ArtistViewHolder) holder).imgArtist.setImageBitmap(artist.getImageArtist());
        } else if (holder instanceof FolderViewHolder){
            Folder folder = (Folder) listData.get(position);
            ((FolderViewHolder) holder).tvUri.setText(folder.getUrl());
            ((FolderViewHolder) holder).tvNameFolder.setText(folder.getNameFolder());
        } else if (holder instanceof TextViewHolder){
            String s = (String) listData.get(position);
            ((TextViewHolder) holder).tvText.setText(s);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (listData.get(position) instanceof Song){
            return ITEM_SONG;
        } else if (listData.get(position) instanceof Album){
            return  ITEM_AlBUM;
        } else if (listData.get(position) instanceof Artist){
            return ITEM_ARTIST;
        } else if (listData.get(position) instanceof Folder){
            return ITEM_FOLDER;
        } else if (listData.get(position) instanceof String){
            return ITEM_TEXT;
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public void setOnItemRecycleViewClick(OnItemRecycleViewClick onItemRecycleViewClick){
        this.onItemRecycleViewClick = onItemRecycleViewClick;
    }

    class SongViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgSong,imgPlay;
        private TextView tvName, tvArtist;

        public SongViewHolder(View itemView) {
            super(itemView);
            imgSong = (ImageView) itemView.findViewById(R.id.imglist);
            imgPlay = (ImageView) itemView.findViewById(R.id.imgIsPlay);
            tvName = (TextView) itemView.findViewById(R.id.tvList1);
            tvArtist = (TextView) itemView.findViewById(R.id.tvList2);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemRecycleViewClick != null){
                        onItemRecycleViewClick.onItemClick(getAdapterPosition());
                    }
                }
            });
        }
    }

    class AlbumViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgAlbum;
        private TextView tvName,tvArtist;

        public AlbumViewHolder(View itemView) {
            super(itemView);
            imgAlbum = (ImageView) itemView.findViewById(R.id.imglist);
            tvName = (TextView) itemView.findViewById(R.id.tvList1);
            tvArtist = (TextView) itemView.findViewById(R.id.tvList2);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemRecycleViewClick != null){
                        onItemRecycleViewClick.onItemClick(getAdapterPosition());
                    }
                }
            });
        }

    }

    class ArtistViewHolder extends RecyclerView.ViewHolder{

        private ImageView imgArtist;
        private TextView tvName,tvNumberAlbumAndTrack;

        public ArtistViewHolder(View itemView) {
            super(itemView);
            imgArtist = (ImageView) itemView.findViewById(R.id.imglist);
            tvName = (TextView) itemView.findViewById(R.id.tvList1);
            tvNumberAlbumAndTrack = (TextView) itemView.findViewById(R.id.tvList2);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemRecycleViewClick != null){
                        onItemRecycleViewClick.onItemClick(getAdapterPosition());
                    }
                }
            });
        }
    }

    class FolderViewHolder extends RecyclerView.ViewHolder{

        private TextView tvNameFolder,tvUri;

        public FolderViewHolder(View itemView) {
            super(itemView);
            tvNameFolder = (TextView) itemView.findViewById(R.id.tvList1);
            tvUri = (TextView) itemView.findViewById(R.id.tvList2);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemRecycleViewClick != null){
                        onItemRecycleViewClick.onItemClick(getAdapterPosition());
                    }
                }
            });
        }
    }

    class TextViewHolder extends RecyclerView.ViewHolder{

        private TextView tvText;

        public TextViewHolder(View itemView) {
            super(itemView);
            tvText = (TextView) itemView.findViewById(R.id.tvText);
        }
    }
}
