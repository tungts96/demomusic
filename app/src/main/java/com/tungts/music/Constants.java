package com.tungts.music;

/**
 * Created by tungts on 8/1/2017.
 */

public class Constants {

    public interface ACTION {
        public static String PREV_ACTION = "action.prev";
        public static String PLAY_ACTION = "action.play";
        public static String PAUSE_ACTION = "action.pause";
        public static String NEXT_ACTION = "action.next";
        public static String SEEKTO_ACTION = "action.seekto";
        public static String STARTFOREGROUND_ACTION = "action.startforeground";
        public static String UPDATE_ACTION = "action.update";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }
}