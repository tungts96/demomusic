package com.tungts.music.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.SeekBar;

import com.tungts.music.interfaces.CurrentItemSong;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.RunnableFuture;

/**
 * Created by tungts on 7/29/2017.
 */

public class MediaPlayerManager implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {

    ArrayList<Song> arrPlayListSongs;
    MediaPlayer mediaPlayer;
    Context context;
    SeekBar songProgressBar;
    int currentIndexSong;
    Handler handler = new Handler();
    CurrentItemSong currentItemSong;

    //binder

    public MediaPlayerManager(SeekBar songProgressBar, Context context) {
        this.songProgressBar = songProgressBar;
        this.context = context;
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(this);
//        mediaPlayer.setOnPreparedListener(this);
//        songProgressBar.setOnSeekBarChangeListener(this);
    }

    public MediaPlayerManager() {
//        this.songProgressBar = songProgressBar;
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(this);
//        mediaPlayer.setOnPreparedListener(this);
//        songProgressBar.setOnSeekBarChangeListener(this);
    }

    //play song
    public void playSong(int indexSong) {
        try {
            Log.e("ST", indexSong + "");
            //play song
            currentIndexSong = indexSong;
            mediaPlayer.reset();
            mediaPlayer.setDataSource(arrPlayListSongs.get(indexSong).getUri());
            mediaPlayer.prepare();
            mediaPlayer.start();
//            mediaPlayer.prepareAsync();

//            //songprogress
//            songProgressBar.setMax(100);
//            songProgressBar.setProgress(0);
//            updateSongProgressBar();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

//    private void updateSongProgressBar() {
//        handler.postDelayed(runnable, 100);
//    }
//
//    Runnable runnable = new Runnable() {
//        @Override
//        public void run() {
//            try {
//                long duration = mediaPlayer.getDuration();
//                long currentDuration = mediaPlayer.getCurrentPosition();
//                double progress = ((double) currentDuration / duration) * 100;
//                songProgressBar.setProgress((int) progress);
//                handler.postDelayed(this, 100);
//            } catch (Exception e){
//
//            }
//
//        }
//    };
//    //next Song
//    public void nextSong() {
//        if (currentIndexSong < arrPlayListSongs.size() - 1) {
//            currentIndexSong++;
//        } else {
//            currentIndexSong = 0;
//        }
//        playSong(currentIndexSong);
//    }
//
//    //previous Song
//    public void previousSong() {
//        if (currentIndexSong > 0) {
//            currentIndexSong -= 1;
//        } else {
//            currentIndexSong = arrPlayListSongs.size() - 1;
//            ;
//        }
//        playSong(currentIndexSong);
//    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public int getCurrentIndexSong() {
        return currentIndexSong;
    }

    public ArrayList<Song> getArrPlayListSongs() {
        return arrPlayListSongs;
    }

    public void setArrPlayListSongs(ArrayList<Song> arrPlayListSongs) {
        this.arrPlayListSongs = arrPlayListSongs;
    }

    public void setOnCompetionMediaPlayer(CurrentItemSong currentItemSong) {
        this.currentItemSong = currentItemSong;
    }

    //when to the end song
    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        Log.e("Music size",arrPlayListSongs.size()+"");
        Log.e("Music com",currentIndexSong+"");
        if (currentIndexSong < arrPlayListSongs.size()-1) {
            currentIndexSong++;
        } else {
            currentIndexSong = 0;
        }
        if (currentItemSong != null) {
            Log.e("Music com",currentIndexSong+"");
            currentItemSong.currentSong(arrPlayListSongs.get(currentIndexSong), currentIndexSong, arrPlayListSongs);
        }

//        mediaPlayer.release();
    }

    //error
    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        mediaPlayer.reset();
        return false;
    }

//    //prepare for start
//    @Override
//    public void onPrepared(MediaPlayer mediaPlayer) {
//        //start playback
//        mediaPlayer.start();
//    }
//
//    @Override
//    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
//    }
//
//    //start touch
//    @Override
//    public void onStartTrackingTouch(SeekBar seekBar) {
////        handler.removeCallbacks(runnable);
//    }
//
//    //stop touch
//    @Override
//    public void onStopTrackingTouch(SeekBar seekBar) {
////        handler.removeCallbacks(runnable);
////        int progress = seekBar.getProgress();
////        int currentDuration = 0;
////        int totalduration = mediaPlayer.getDuration();
////        currentDuration = (int) ((((double) progress) / 100) * totalduration);
////        mediaPlayer.seekTo(currentDuration);
////        updateSongProgressBar();
//    }

//    private final IBinder musicBind = new MusicBinder();
//
//    public class MusicBinder extends Binder {
//        public MediaPlayerManager getService() {
//            return MediaPlayerManager.this;
//        }
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        Log.e("Music","Create");
//    }
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        Log.e("Music","onBind");
//        return musicBind;
//    }
//
//    @Override
//    public boolean onUnbind(Intent intent) {
//        Log.e("Music","onUnbind");
//        mediaPlayer.stop();
//        mediaPlayer.release();
//        return false;
//    }
//
//    @Override
//    public void onDestroy() {
//        Log.e("Music","onDestroy");
//        super.onDestroy();
//        stopForeground(true);
//    }

}
