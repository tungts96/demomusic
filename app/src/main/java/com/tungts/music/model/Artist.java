package com.tungts.music.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;

import java.io.Serializable;

/**
 * Created by tungts on 7/27/2017.
 */

public class Artist implements Serializable {

    String nameArtist;
    int numberOfAlbums;
    int numberOfTracks;
    String artistKey;
    Bitmap imageArtist;

    public Artist(String nameArtist, int numberOfAlbums, int numberOfTracks, String artistKey) {
        this.nameArtist = nameArtist;
        this.numberOfAlbums = numberOfAlbums;
        this.numberOfTracks = numberOfTracks;
        this.artistKey = artistKey;
    }

    public String getNameArtist() {
        return nameArtist;
    }

    public void setNameArtist(String nameArtist) {
        this.nameArtist = nameArtist;
    }

    public int getNumberOfAlbums() {
        return numberOfAlbums;
    }

    public void setNumberOfAlbums(int numberOfAlbums) {
        this.numberOfAlbums = numberOfAlbums;
    }

    public int getNumberOfTracks() {
        return numberOfTracks;
    }

    public void setNumberOfTracks(int numberOfTracks) {
        this.numberOfTracks = numberOfTracks;
    }

    public String getArtistKey() {
        return artistKey;
    }

    public void setArtistKey(String artistKey) {
        this.artistKey = artistKey;
    }

    public void setImageArtist(Bitmap image){
        this.imageArtist = image;
    }

    public Bitmap getImageArtist(){
        return imageArtist;
    }
}
