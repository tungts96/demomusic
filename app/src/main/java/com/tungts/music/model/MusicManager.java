package com.tungts.music.model;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by tungts on 7/27/2017.
 */

public class MusicManager {

    private Context context;

    public MusicManager(Context context){
        this.context = context;
    }

    //get list song
    public List<Song> getListSongsFromStroage(){
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

        List<Song> listSongs = new ArrayList<>();
        //get uri query to song table
        Cursor c = context.getContentResolver().query(uri,null,selection,null,null);
        if (c == null || c.getCount() == 0){
            return listSongs;
        }
        int indexUri = c.getColumnIndex(MediaStore.Audio.Media.DATA);
        int indexName = c.getColumnIndex(MediaStore.Audio.Media.TITLE);
        int indexArtist = c.getColumnIndex(MediaStore.Audio.Media.ARTIST);
        int indexAlbum = c.getColumnIndex(MediaStore.Audio.Media.ALBUM);
//        int indexComposer = c.getColumnIndex(MediaStore.Audio.Media.COMPOSER);
        int indexDate = c.getColumnIndex(MediaStore.Audio.Media.DATE_ADDED);
        int indexDuration = c.getColumnIndex(MediaStore.Audio.Media.DURATION);
        int indexArtistKey = c.getColumnIndex(MediaStore.Audio.Media.ARTIST_KEY);
        int indexAlbumKey = c.getColumnIndex(MediaStore.Audio.Media.ALBUM_KEY);
        c.moveToFirst();

        while (!c.isAfterLast()){
            String path = c.getString(indexUri);
            String name = c.getString(indexName);
            String artist = c.getString(indexArtist);
            String album = c.getString(indexAlbum);
//            String composer = c.getString(indexComposer);
            String date = c.getString(indexDate);
            int duration = c.getInt(indexDuration);
            String albumKey = c.getString(indexAlbumKey);
            String artistKey = c.getString(indexArtistKey);
            listSongs.add(new Song(path,name,artist,album,date,duration,albumKey,artistKey));
            c.moveToNext();
        }
        return listSongs;
    }

    //get List Albums
    public List<Album> getListAlbums(){
        List<Album> listAlbums = new ArrayList<>();
        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        Cursor c = context.getContentResolver().query(uri,null,null,null,null);
        if (c == null || c.getCount() == 0){
            return listAlbums;
        }

        int indexIdAlbum = c.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ID);
        int indexArtAlbum = c.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART);
        int indexNameAlbum = c.getColumnIndex(MediaStore.Audio.Albums.ALBUM);
        int indexnameArtist = c.getColumnIndex(MediaStore.Audio.Albums.ARTIST);
        int indexnumberOfSong= c.getColumnIndex(MediaStore.Audio.Albums.NUMBER_OF_SONGS);
        int indexfirstYear = c.getColumnIndex(MediaStore.Audio.Albums.FIRST_YEAR);
        int indexAlbumKey = c.getColumnIndex(MediaStore.Audio.Albums.ALBUM_KEY);
        c.moveToFirst();

        while (!c.isAfterLast()){
            String nameAlbum = c.getString(indexNameAlbum);
            String nameArtist = c.getString(indexnameArtist);
            int numberOfSong = c.getInt(indexnumberOfSong);
            int firstYear = c.getInt(indexfirstYear);
            String albumkey = c.getString(indexAlbumKey);
            String imageAlbum = c.getString(indexArtAlbum);
            Album album = new Album(nameAlbum,nameArtist,numberOfSong,firstYear,albumkey);
            album.setImageAlbum(imageAlbum);
            listAlbums.add(album);
            c.moveToNext();
        }
        return listAlbums;
    }

    //get List Artist
    public List<Artist> getListArtists(){
        List<Artist> artists = new ArrayList<>();
        Uri  uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;
        Cursor c = context.getContentResolver().query(uri, null, null, null, null);
        if (c == null || c.getCount() == 0){
            return artists;
        }
        int indexNameArtist = c.getColumnIndex(MediaStore.Audio.Artists.ARTIST);
        int indexNumberOfAlbum = c.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_ALBUMS);
        int indexNumberOfTrack = c.getColumnIndex(MediaStore.Audio.Artists.NUMBER_OF_TRACKS);
        int indexKeyArtist = c.getColumnIndex(MediaStore.Audio.Artists.ARTIST_KEY);
        c.moveToFirst();

        while (!c.isAfterLast()){
            String  nameArtist = c.getString(indexNameArtist);
            int numberOfAlbum = c.getInt(indexNumberOfAlbum);
            int numberOfTrack = c.getInt(indexNumberOfTrack);
            String artistKey = c.getString(indexKeyArtist);
            Artist artist = new Artist(nameArtist,numberOfAlbum,numberOfTrack,artistKey);
            artists.add(artist);
            c.moveToNext();
        }

        return artists;
    }

    //get List folder
    public List<Folder> getListFolder(){
        List<Folder> listFolders = new ArrayList<>();
        HashMap<String,ArrayList<Song>> map = new HashMap<>();
        ArrayList<Song>  songs = (ArrayList<Song>) getListSongsFromStroage();
        if (songs.size() == 0) return listFolders;
        String s = songs.get(0).getUri();
        int lastIndex = s.lastIndexOf("/");
        s = s.substring(0,lastIndex);
        map.put(s,null);
        for (Song song:songs){
            String uri = song.getUri();
            int index = uri.lastIndexOf("/");
            uri = uri.substring(0,index);
            if (map.get(uri) != null){
                map.get(uri).add(song);
            } else {
                ArrayList<Song> a = new ArrayList<>();
                a.add(song);
                map.put(uri,a);
            }
        }
        for (String urifolder : map.keySet()){
            listFolders.add(new Folder(urifolder,map.get(urifolder)));
        }
        return listFolders;
    }

    public ArrayList<Song> getListSongFromAlbum(Album album){
        ArrayList<Song> list = new ArrayList<>();
        ArrayList<Song> songs = (ArrayList<Song>) getListSongsFromStroage();
        for (Song song:songs){
            if (song.getNameAlbum().equals(album.getNameAlbum())){
                list.add(song);
            }
        }
        return list;
    }

    public ArrayList<Song> getListSongFromArtist(Artist artist){
        ArrayList<Song> list = new ArrayList<>();
        ArrayList<Song> songs = (ArrayList<Song>) getListSongsFromStroage();
        for (Song song:songs){
            if (song.getNameArtist().equals(artist.getNameArtist())){
                list.add(song);
            }
        }
        return list;
    }

    public ArrayList<Album> getListAlbumFromArtist(Artist artist) {
        ArrayList<Album> list = new ArrayList<>();
        ArrayList<Album> albums = (ArrayList<Album>) getListAlbums();
        for (Album album:albums){
            if (album.getNameArtist().equals(artist.getNameArtist())){
                Log.e("ST",album.getNameArtist() + " "+artist.getNameArtist() );
                list.add(album);
            }
        }
        return list;
    }
}
