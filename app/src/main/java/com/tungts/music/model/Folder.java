package com.tungts.music.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by tungts on 7/27/2017.
 */

public class Folder implements Serializable{

    private String url;
    private ArrayList<Song> listSongs;

    public Folder(String url, ArrayList<Song> listSongs) {
        this.url = url;
        this.listSongs = listSongs;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<Song> getListSongs() {
        return listSongs;
    }

    public void setListSongs(ArrayList<Song> listSongs) {
        this.listSongs = listSongs;
    }

    public String getNameFolder(){
        int lastIndex = this.url.lastIndexOf("/");
        return url.substring(lastIndex+1,url.length());

    }
}
