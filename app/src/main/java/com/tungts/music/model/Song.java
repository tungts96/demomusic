package com.tungts.music.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaDataSource;
import android.media.MediaMetadata;
import android.media.MediaMetadataRetriever;

import java.io.Serializable;

/**
 * Created by tungts on 7/27/2017.
 */

public class Song implements Serializable{

    private String uri;
    private String nameSong;
    private String nameArtist;
    private String nameAlbum;
    private String dateAdded;
    private int duration;
    private String albumKey;
    private String artistKey;
    private boolean isPlay;

    public Song(String uri, String nameSong, String artist, String nameAlbum, String date, int duration,String albumKey,String artistKey) {
        this.uri = uri;
        this.nameSong = nameSong;
        this.nameArtist = artist;
        this.nameAlbum = nameAlbum;
        this.dateAdded = date;
        this.duration = duration;
        this.albumKey = albumKey;
        this.artistKey = artistKey;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getNameSong() {
        return nameSong;
    }

    public void setNameSong(String nameSong) {
        this.nameSong = nameSong;
    }

    public String getNameArtist() {
        return nameArtist;
    }

    public void setNameArtist(String nameArtist) {
        this.nameArtist = nameArtist;
    }

    public String getAlbum() {
        return nameAlbum;
    }

    public void setAlbum(String album) {
        this.nameAlbum = album;
    }


    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getNameAlbum() {
        return nameAlbum;
    }

    public void setNameAlbum(String nameAlbum) {
        this.nameAlbum = nameAlbum;
    }

    public String getAlbumKey() {
        return albumKey;
    }

    public void setAlbumKey(String albumKey) {
        this.albumKey = albumKey;
    }

    public String getArtistKey() {
        return artistKey;
    }

    public void setArtistKey(String artistKey) {
        this.artistKey = artistKey;
    }

    public boolean isPlay() {
        return isPlay;
    }

    public void setPlay(boolean play) {
        isPlay = play;
    }

    public Bitmap getImage(){
        MediaMetadataRetriever dataRetriver = new MediaMetadataRetriever();
        dataRetriver.setDataSource(getUri());

        byte[] data = dataRetriver.getEmbeddedPicture();
        if (data != null){
            return BitmapFactory.decodeByteArray(data,0,data.length);
        }
        return  null;
    }
}
