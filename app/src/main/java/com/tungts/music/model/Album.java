package com.tungts.music.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.Serializable;

/**
 * Created by tungts on 7/27/2017.
 */

public class Album implements Serializable {

    String nameAlbum;
    String nameArtist;
    int numberOfSong;
    int firstYear;
    String albumKey;
    String imageAlbum;

    public Album(String nameAlbum, String nameArtist, int numberOfSong, int firstYear, String albumKey) {
        this.nameAlbum = nameAlbum;
        this.nameArtist = nameArtist;
        this.numberOfSong = numberOfSong;
        this.firstYear = firstYear;
        this.albumKey = albumKey;
    }

    public String getNameAlbum() {
        return nameAlbum;
    }

    public void setNameAlbum(String nameAlbum) {
        this.nameAlbum = nameAlbum;
    }

    public String getNameArtist() {
        return nameArtist;
    }

    public void setNameArtist(String nameArtist) {
        this.nameArtist = nameArtist;
    }

    public int getNumberOfSong() {
        return numberOfSong;
    }

    public void setNumberOfSong(int numberOfSong) {
        this.numberOfSong = numberOfSong;
    }

    public int getFirstYear() {
        return firstYear;
    }

    public void setFirstYear(int firstYear) {
        this.firstYear = firstYear;
    }

    public String getAlbumKey() {
        return albumKey;
    }

    public void setAlbumKey(String albumKey) {
        this.albumKey = albumKey;
    }

    public void setImageAlbum(String image){
        this.imageAlbum = image;
    }

    public Bitmap getImageAlbum(){
        return BitmapFactory.decodeFile(imageAlbum);
    }
}
