package com.tungts.music.interfaces;

/**
 * Created by tungts on 7/28/2017.
 */

public interface OnItemRecycleViewClick {
    void onItemClick(int position);
}
