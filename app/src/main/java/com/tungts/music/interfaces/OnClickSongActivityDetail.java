package com.tungts.music.interfaces;

import com.tungts.music.model.Song;

import java.util.ArrayList;

/**
 * Created by tungts on 8/3/2017.
 */

public interface OnClickSongActivityDetail {
    void itemSong(int pos, ArrayList<Song> arrSongs);
}
