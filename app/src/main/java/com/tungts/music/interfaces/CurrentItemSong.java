package com.tungts.music.interfaces;

import com.tungts.music.model.Song;

import java.util.ArrayList;

/**
 * Created by tungts on 7/30/2017.
 */

public interface CurrentItemSong {
    void currentSong(Song song, int pos, ArrayList<Song> songs);
}
