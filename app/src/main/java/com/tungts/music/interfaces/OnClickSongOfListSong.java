package com.tungts.music.interfaces;

import com.tungts.music.model.Song;

import java.util.ArrayList;

/**
 * Created by tungts on 7/28/2017.
 */

public interface OnClickSongOfListSong {
    void itemSongOfMainActivity(int pos, ArrayList<Song> arrSongs);
}
